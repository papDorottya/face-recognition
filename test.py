import cv2
import sys
import train as fd

imagePath = sys.argv[1]
image = cv2.imread(imagePath)

face_det, gray = fd.faceDetection(image)

# For Training
# faces, faceID = fd.labels_for_training_data('datasets')
# model = fd.train_classifier(faces, faceID)
# model.save('trainData.yml')

name = {0: "Barack Obama", 1: "Yang Mi"}

# For Testing
model = cv2.face.LBPHFaceRecognizer_create()
model.read('trainData.yml')

for face in face_det:
    (x, y, w, h) = face
    roi_gray = gray[y:y + h, x:x + h]
    label, confidence = model.predict(roi_gray)  # predicting the label of given image
    fd.draw_rect(image, face)
    predicted_name = name[label]
    if (confidence > 51):
        continue
    fd.put_text(image, predicted_name, x, y)

cv2.imshow("Face Detection", image)
cv2.waitKey(0)
cv2.destroyAllWindows
