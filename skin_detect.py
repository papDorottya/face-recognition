import sys
import cv2
import numpy as np
import matplotlib.pyplot as plt

class Skin_Detect():
    def __init__(self):
        pass

    def RGB(self, RGB_Frame, plot=False):
        B_Frame, G_Frame, R_Frame  = cv2.split(RGB_Frame)

        BRG_Max = np.maximum.reduce([B_Frame, G_Frame, R_Frame])
        BRG_Min = np.minimum.reduce([B_Frame, G_Frame, R_Frame])

        # Uniform daylight illumination conditions
        Rule_1 = np.logical_and.reduce([R_Frame > 95, G_Frame > 40, B_Frame > 20,
                                        BRG_Max - BRG_Min > 15, abs(R_Frame - G_Frame) > 15,
                                        R_Frame > G_Frame, R_Frame > B_Frame])

        # Flashlight or daylight lateral illumination conditions
        Rule_2 = np.logical_and.reduce([R_Frame > 220, G_Frame > 210, B_Frame > 170,
                                        abs(R_Frame - G_Frame) <= 15, R_Frame > B_Frame, G_Frame > B_Frame])

        # Rule_1 OR Rule_2
        RGB_Rule = np.logical_or(Rule_1, Rule_2)
        if plot == True:
            fig = plt.figure(figsize=(9, 8), dpi=90, facecolor='w', edgecolor='k')
            fig.suptitle('RGB Mask', fontsize=40)

            img_bw = RGB_Rule.astype(np.uint8)
            img_bw *= 255
            ax1 = fig.add_subplot(1, 1, 1)
            ax1.axes.get_xaxis().set_visible(False)
            ax1.axes.get_yaxis().set_visible(False)

            ax1.imshow(img_bw, cmap='gray', vmin=0, vmax=255, interpolation='nearest')

        return RGB_Rule

    def computeDelimiters(self, axis):
        line1 = 1.5862 * axis + 20
        line2 = 0.3448 * axis + 76.2069
        line3 = -1.005 * axis + 234.5652
        line4 = -1.15 * axis + 301.75
        line5 = -2.2857 * axis + 432.85

        return [line1, line2, line3, line4, line5]

    def YCrCb(self, YCrCb_Frame, plot=False):
        Y_Frame, Cr_Frame, Cb_Frame = cv2.split(YCrCb_Frame)

        line1, line2, line3, line4, line5 = self.computeDelimiters(Cb_Frame)
        YCrCb_Rule = np.logical_and.reduce([line1 - Cr_Frame >= 0,
                                            line2 - Cr_Frame <= 0,
                                            line3 - Cr_Frame <= 0,
                                            line4 - Cr_Frame >= 0,
                                            line5 - Cr_Frame >= 0])

        if plot == True:
            fig1 = plt.figure(figsize=(9, 8), dpi=90, facecolor='w', edgecolor='k')
            fig1.suptitle('YCrCb Mask', fontsize=40)

            img_bw = YCrCb_Rule.astype(np.uint8)
            img_bw *= 255
            ax1 = fig1.add_subplot(1, 1, 1)
            ax1.axes.get_xaxis().set_visible(False)
            ax1.axes.get_yaxis().set_visible(False)

            ax1.imshow(img_bw, cmap='gray', vmin=0, vmax=255, interpolation='nearest')
        return YCrCb_Rule

    def HSV(self, HSV_Frame, plot=False):
        Hue, Sat, Val = [HSV_Frame[..., i] for i in range(3)]

        HSV_ = np.logical_or(Hue < 50, Hue > 150)
        if plot == True:
            fig2 = plt.figure(figsize=(9, 8), dpi=90, facecolor='w', edgecolor='k')
            fig2.suptitle('Hue Mask', fontsize=40)

            Hue_bw = HSV_.astype(np.uint8)
            Hue_bw *= 255
            ax1 = fig2.add_subplot(1, 1, 1)
            ax1.axes.get_xaxis().set_visible(False)
            ax1.axes.get_yaxis().set_visible(False)

            ax1.imshow(Hue_bw, cmap='gray', vmin=0, vmax=255, interpolation='nearest')
        return HSV_

    def RGB_YCbCr_HSV(self, Frame_, plot=False):
        Ycbcr_Frame = cv2.cvtColor(Frame_, cv2.COLOR_BGR2YCrCb)
        HSV_Frame = cv2.cvtColor(Frame_, cv2.COLOR_BGR2HSV)

        skin_ = np.logical_and.reduce([self.RGB(Frame_), self.YCrCb(Ycbcr_Frame), self.HSV(HSV_Frame)])
        if plot == True:
            skin_bw = skin_.astype(np.uint8)
            skin_bw *= 255

            RGB_Frame = cv2.cvtColor(Frame_, cv2.COLOR_BGR2RGB)
            seg = cv2.bitwise_and(Frame_, Frame_, mask=skin_bw)

            cv2.imshow("Result", seg)
        return np.asarray(skin_, dtype=np.uint8)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Please choose an image!")
        sys.exit(0)
    image = sys.argv[1]
    try:
        img = np.array(cv2.imread(image), dtype=np.uint8)
    except:
        print("Error while loading the image!")
        sys.exit(1)
    test = Skin_Detect()
    YCrCb_Frames = cv2.cvtColor(img, cv2.COLOR_BGR2YCrCb)
    HSV_Frames = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)
    test.RGB(img, True)
    test.YCrCb(YCrCb_Frames, True)
    test.HSV(HSV_Frames, True)
    test.RGB_YCbCr_HSV(img, True)
    plt.show()
    cv2.waitKey(0)