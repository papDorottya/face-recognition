import argparse as arg
import time
import cv2
import numpy as np
from skin_detect import *


class Face_Detector():
    def __init__(self, skin_detect):
        self._skin_detect = skin_detect

    @property
    def skin_detect(self):
        return self._skin_detect

    def Detect_Face_Img(self, img, size1, size2):
        skin_img = self._skin_detect.RGB_YCbCr_HSV(img, False)
        contours, hierarchy = cv2.findContours(skin_img, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        rects = []
        for c in contours:
            x, y, w, h = cv2.boundingRect(c)
            # draw a green rectangle to visualize the bounding rect
            if (w > size1[0] and h > size1[1]) and (w < size2[0] and h < size2[1]):
                # pinhole distance
                Distance1 = 11.5 * (img.shape[1] / float(w))
                # camera distance
                Distance2 = 15.0 * ((img.shape[1] + 226.8) / float(w))
                print("\npinhole distance = {:.2f} cm\ncamera distance = {:.2f} cm".format(Distance1, Distance2))
                print("Width = {} \t Height = {}".format(w, h))
                rects.append(np.asarray([x, y, w, w * 1.25], dtype=np.uint16))
        return rects


def Arg_Parser():
    Arg_Par = arg.ArgumentParser()
    Arg_Par.add_argument("image",
                         help="relative/absolute path of the image file")
    arg_list = vars(Arg_Par.parse_args())
    return arg_list


def open_img(arg_):
    mg_src = arg_["image"]
    img = cv2.imread(mg_src)
    img_arr = np.array(img, 'uint8')
    return img_arr


if __name__ == "__main__":
    if len(sys.argv) == 1:
        print("Please give an image!")
        sys.exit(0)
    in_arg = Arg_Parser()
    skin_detect = Skin_Detect()
    size1 = (40, 40)
    size2 = (300, 400)
    scale_factor = 3
    Face_Detect = Face_Detector(skin_detect)

    img = open_img(in_arg)
    rects = Face_Detect.Detect_Face_Img(img, size1, size2)
    print(rects)
    for i, r in enumerate(rects):
        x, y, w, h = r
        cv2.rectangle(img, (x, y), (x + w, y + h), (0, 255, 0), 1)
    cv2.imshow("faces", img)
    if cv2.waitKey(0) & 0xFF == ord("q"):
        sys.exit(0)
